package net.tngou.jtdb.example;

import net.tngou.jtdb.TngouDBHelp;
import net.tngou.jtdb.netty.TngouClient;

public class Table {

	public static void main(String[] args) {
		
		TngouDBHelp dbHelp = TngouDBHelp.getConnection();  //建立连接
		dbHelp.createTable("tngou");  //创建表
		dbHelp.dropTable("tngou");   //删除表
		dbHelp.closeConnection();   //回收连接 回收到连接池
		String sql="create table tg";
		dbHelp.execute(sql);              //执行SQL语句
		sql="drop table tg";
		dbHelp.execute(sql);
		dbHelp.closeConnection();   //回收连接 回收到连接池
		
		dbHelp.close();            //关闭连接…… 一般情况下不需要关闭   ；

	}

}
