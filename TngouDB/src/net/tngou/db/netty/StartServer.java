package net.tngou.db.netty;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.tngou.db.lucene.Config;
import net.tngou.db.lucene.LuceneManage;
import net.tngou.db.util.I18N;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class StartServer extends Netty implements Runnable {

	private static Logger log = LoggerFactory.getLogger(StartServer.class);
	 private int port=Integer.parseInt(System.getProperty("port", "8788"));;  
     
	 public StartServer(int port) {  
	       
		 this.port = port;  
	 } 
	 
	public void run()  {
		
//		 EventLoopGroup bossGroup = new NioEventLoopGroup(10);
//		 EventLoopGroup workerGroup = new NioEventLoopGroup();	
		
		
		
		 try {
			 
			 
			 Parameters params = new Parameters();
				FileBasedConfigurationBuilder<?> builder =
				    new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
				    .configure(params.properties()
				        .setFileName("config.properties"));
				Configuration config = builder.getConfiguration();

				config.setProperty("run", 1);  //设置启动标示
				builder.save();
			 
			// Server服务启动器  
				 ServerBootstrap b = new ServerBootstrap();
				 b.group(bossGroup, workerGroup)
				 .channel(NioServerSocketChannel.class)			
//				 .handler(new LoggingHandler(LogLevel.INFO))
//				 .childHandler(new ServerInitializer())
				 .option(ChannelOption.SO_BACKLOG, 120)
				 .childOption(ChannelOption.SO_KEEPALIVE, true) // (6)
				  .childHandler(new ChannelInitializer<SocketChannel>() {
					@Override
					public void initChannel(SocketChannel ch) {
					ChannelPipeline p = ch.pipeline();
					 
					 p.addLast(new ObjectEncoder());
					 p.addLast( new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
				  	 p.addLast(new ServerHandler());
					}
					});
				 
				 ch  = b.bind(port).sync().channel();
				 ch.closeFuture().sync();
				
			
			 } catch (InterruptedException | ConfigurationException e) {
				 log.error(I18N.getValue("start_error",port+""));
				e.printStackTrace();
				
			} finally {
				workerGroup.shutdownGracefully();
				bossGroup.shutdownGracefully();
				Thread.yield();
			 }
	}
	
	public static void main(String[] args) {  

		 int port =  Config.getInstance().getPort();
         Thread t = new Thread(new StartServer(port));
         t.start();  
         
         log.info(I18N.getValue("start",port+""));
         Thread td = new Thread(new Daemons());
         td.setDaemon(true);  //设置后台监听程序
         td.start();
//        new TngouServer(port).run();  
//        stop();
       
    } 
}
