package net.tngou.db.entity;


import java.util.List;
import java.util.Map;


/**
 * 
* @ClassName: Page
* @Description: TODO 返回数据
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月20日 下午2:46:44
*
 */
public class Page 
{
	 private List<Map<String, Object>> list; //数据集
	 private int page;//当前页
	 private int start;//开始条数
	 private int size;//数据大小
	 private int totalpage;//总页数
	 private int total;//总条数

	 public Page() {
		// TODO Auto-generated constructor stub
	}
	 
	 public Page(int start ,int size ,int total,List<Map<String, Object>> list) 
	 {
			this.page =start/size+1;
			this.size = size;
			this.total= total;
			this.totalpage = total%size==0?total/size:(total/size+1);
			this.start=start;
			
			this.list = list;
		}
	 
	 
	 
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotalpage() {
		return totalpage;
	}
	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}



	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	
	 

}
